// Déclaration des constantes joueurs
const player = Joueur
const dealer = Croupier

// Déclaration de la variable carte 
let carte

// Fontion permettant de choisir un carte alétoire basé sur la valeur
// entre 1 & 14 pour refleter les 10 cartes "numérales" et les 3 cartes "figures"
// Si le numbre est > à 10 alors la valeur de la carte est ramené à 10
function getRandomCard (max) {
    carte = Math.floor(Math.random() * max)
    if (carte >= 10) {
        carte = 10
    }
}

btnAction = document.getElementById("btnAction")
console.log(btnAction)
btnAction.addEventListener("click", () => {
    getRandomCard(14)
})